import logging
from dataclasses import dataclass

import requests
import semver

from .bootstrap_model import APIObject, System, ApiFunction


@dataclass()
class Loader(object):
    api_scheme: str = 'https'
    api_host: str = 'api.netdb.scc.kit.edu'
    doku_host: str = 'doku.netdb.scc.kit.edu'
    api_version: str = '4.0'
    version_detail: dict = None
    base_dir: str = ''
    _auth: str = None
    sess: requests.Session = requests.Session()
    api_functions = None
    systems = None
    data_types = None
    api_objects = None
    lang: str = 'de'

    def __post_init__(self):
        if self._auth is not None:
            self.sess.headers.update({'Authorization': 'Bearer ' + str(self.auth)})

    @property
    def auth(self):
        return self._auth

    @auth.setter
    def auth(self, auth):
        self._auth = auth
        if auth is None:
            self.sess.headers.pop('Authorization')
        else:
            self.sess.headers.update({'Authorization': 'Bearer ' + str(auth)})

    @property
    def api_base_url(self):
        return f'/{self.api_version}'

    def load(self):
        logging.info(f'loading version index')
        version_req = self.sess.get("{scheme}://{base_host}".format(scheme=self.api_scheme,
                                                                    base_host=self.api_host))
        for v in version_req.json()[0]:
            if f'{v["major"]}.{v["minor"]}' == self.api_version:
                self.version_detail = v
                break
        if self.version_detail is None:
            raise ValueError(f'Unsupported API version "{self.api_version}"')
        logging.info(f'{self.api_version}: loading functions')
        functions_req = self.sess.get(
            "{scheme}://{base_host}{base_url}/wapi/function/list".format(scheme=self.api_scheme,
                                                                         base_host=self.api_host,
                                                                         base_url=self.api_base_url))
        if not functions_req.status_code == 200:
            logging.fatal(f"Fail! Unexpected status_code {functions_req.status_code}. DB maintenance?")
            logging.fatal(functions_req.text)
            exit(1)
        func_index = functions_req.json()[0]

        api_system = "wapi"

        logging.info(f'{self.api_version}: loading data types')
        self.data_types = {k['name']: k for k in self.sess.get(
            "{scheme}://{base_host}{base_url}/{system}/data_type/list".format(scheme=self.api_scheme,
                                                                              system=api_system,
                                                                              base_host=self.api_host,
                                                                              base_url=self.api_base_url)).json()[0]}
        logging.info(f'{self.api_version}: loading api objects')
        self.api_objects = {f"{k['system']}.{k['name']}": APIObject(__data_types__=self.data_types, **k) for k in
                            self.sess.get(
                                "{scheme}://{base_host}{base_url}/wapi/object_type/list".format(scheme=self.api_scheme,
                                                                                                base_host=self.api_host,
                                                                                                base_url=self.api_base_url)).json()[
                                0]}
        self.api_functions = [ApiFunction(__data_types__=self.data_types, **f) for f in func_index]
        logging.info(f'{self.api_version}: loading systems')
        self.systems = [System(**s) for s in self.sess.get(
            "{scheme}://{base_host}{base_url}/".format(scheme=self.api_scheme, base_host=self.api_host,
                                                       base_url=self.api_base_url)).json()[0]]
        if semver.Version.parse('4.1.0') <= semver.Version.parse(self.api_version + '.0'):
            logging.info(f'{self.api_version}: loading api translations')
            ta = [
                {
                    'idx': 'object_type_lang',
                    'name': 'wapi.object_type_lang_dict.list',
                },
                {
                    'idx': 'object_type_attr_lang',
                    'name': 'wapi.object_type_attr_lang_dict.list',
                }
            ]
            lang = self.sess.post("{scheme}://{base_host}{base_url}/wapi/transaction/execute?dict_mode=true".format(
                scheme=self.api_scheme,
                base_host=self.api_host,
                base_url=self.api_base_url),
                json=ta)
            lang = lang.json()
            obj_type_lang_by_fq_name = {k['fq_name']: k for k in lang['object_type_lang']}
            obj_type_attr_lang_by_fq_name = {k['fq_name']: k for k in lang['object_type_attr_lang']}
            for k in self.api_objects.values():
                for attr, v in obj_type_lang_by_fq_name[k.fq_name]['language_dict'].items():
                    setattr(k, attr, v[self.lang])
                for attr_name, attr in k.attributes.items():
                    for l_attr, v in obj_type_attr_lang_by_fq_name[f'{k.fq_name}.{attr_name}']['language_dict'].items():
                        setattr(attr, l_attr, v[self.lang])
            for f in self.api_functions:
                for p_name, p in f.parameters.items():
                    for l_attr, v in obj_type_attr_lang_by_fq_name[f'{f.system}.{f.object_type}.{p_name}']['language_dict'].items():
                        setattr(p, l_attr, v[self.lang])