import logging
import os
import re
import sys
from json import JSONEncoder

import click
import yaml

from .bootstrap_model import *
from .loader import Loader

loader = Loader()


class MyEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, APIBaseObject):
            return o.__dict__
        return JSONEncoder.default(self, o)


@click.group()
@click.option('--endpoint', envvar='NETDB_ENDPOINT')
@click.option('--version', envvar='NETDB_VERSION')
@click.option('--token', envvar='NETDB_TOKEN')
@click.option('--lang', envvar='NETDB_LANG')
def cli(endpoint, version, token, lang):
    if endpoint is not None:
        loader.api_host = endpoint
    if version is not None:
        loader.api_version = version
    if token is not None:
        loader.auth = token
    if lang is not None:
        loader.lang = lang
    loader.__post_init__()
    loader.load()


@cli.command()
@click.option('--strict/--skip-errors', default=True)
@click.option('--output-dir', default='')
def python(strict, output_dir):
    keyword_replacements = {'class': 'cls'}

    def camel_case(word):
        parts = word.split('_')
        return ''.join([p.capitalize() for p in parts])

    def gen_def(p, scope):
        if p.__dict__[scope].data_default is not None:
            return f' = {quote_def(p, p.__dict__[scope])}'
        if not p.__dict__[scope].is_required:
            return ' = None'
        return ''

    def replace_keywords(k):
        for t, s in keyword_replacements.items():
            k = k.replace(t, s)
        return k

    def quote_def(p, scope):
        if translate_json_data_type(p.data_type['json_name']) == 'str':
            return f"'{scope.data_default}'"
        return scope.data_default

    def translate_json_data_type(t):
        if t == 'boolean':
            return 'bool'
        if t == 'number':
            return 'int'
        if t == 'string':
            return 'str'
        if t == 'array':
            return 'list'
        if t == 'object':
            return 'dict'
        if t == 'json':
            return 'str'
        raise ValueError(t)

    def generate_object_type_checks(v):
        res = ''
        for p in v.attributes.values():
            res += f"""\t\tif {f"self.{replace_keywords(p.name)} is not None and " if p.is_nullable else ""}not isinstance(self.{replace_keywords(p.name)}, {translate_json_data_type(p.data_type['json_name'])}):
\t\t\traise ValueError(f"'{replace_keywords(p.name)}' of incorrect type. Expected {translate_json_data_type(p.data_type['json_name'])}, got {{type(self.{replace_keywords(p.name)})}}")\n"""
        return res

    def generate_function_type_checks(f):
        res = ''.join([
            f"""\t\tif {f"{replace_keywords(p.name)}_new is not None and " if not p.new.is_required else ""}not (isinstance({replace_keywords(p.name)}_new, {translate_json_data_type(p.data_type['json_name'])}){f" or isinstance({replace_keywords(p.name)}_new, ExplicitNull)" if p.new.is_nullable else ""}):
\t\t\traise ValueError(f"'{replace_keywords(p.name)}' of incorrect type. Expected {translate_json_data_type(p.data_type['json_name'])}, got {{type({replace_keywords(p.name)}_new)}}")\n"""
            for p in
            f.parameters.values() if p.new is not None])
        res += ''.join([
            f"""\t\tif {f"{replace_keywords(p.name)}_old is not None and " if not p.old.is_required else ""}not (isinstance({replace_keywords(p.name)}_old, {translate_json_data_type(p.data_type['json_name'])}){f" or isinstance({replace_keywords(p.name)}_old, ExplicitNull)" if p.old.is_nullable else ""}):
\t\t\traise ValueError(f"'{replace_keywords(p.name)}' of incorrect type. Expected {translate_json_data_type(p.data_type['json_name'])}, got {{type({replace_keywords(p.name)}_old)}}")\n"""
            for p in
            f.parameters.values() if p.old is not None])
        return res

    if not os.path.isdir(output_dir):
        raise click.UsageError(f"Directory '{output_dir}' does not exist.")

    with open(os.path.join(output_dir, '__init__.py'), 'w+') as initfile:
        initfile.write(f"""### AUTOGENERATED
import requests
import os
from typing import Union
from pprint import pformat
import logging

logger = logging.getLogger(__name__)

# ExeccutionError is derived from ValueError to keep backward compatibility.
class ExecutionError(ValueError):
\t\"\"\"Raise if API returns an error code != 400\"\"\"

\tdef __init__(self, status_code, reason, result):
\t\tself.status_code = status_code
\t\tself.reason = reason
\t\tself.result = result
\t\tmessage = f'Request returned unexpected result ({{self.status_code}}: {{self.reason}}). Result: \\n{{pformat(self.result)}}'
\t\tsuper().__init__(message)


class APIObject(object):
\tpass


class ExplicitNull(object):
\tpass


class APIEndpoint(object):
\tdef __init__(self, netdb_base_url:str=None, netdb_token:str=None, **kwargs):
\t\tself.base_url = netdb_base_url if netdb_base_url is not None else kwargs.get('base_url', None)
\t\tself.version = '{loader.api_version}'
\t\tself.token = netdb_token if netdb_token is not None else kwargs.get('token', None)


class APISession(object):
\tdef __init__(self, endpoint: APIEndpoint, protocol: str='https', update_check: bool='NETDB_CLIENT_DISABLE_UPDATE_CHECK' not in os.environ):
\t\tself._build_version = '{loader.version_detail['numeric']}'
\t\tself.session = requests.session()
\t\tself.session.headers.update({{'Authorization': 'Bearer ' + endpoint.token}})
\t\tself.api_root = f'{{protocol}}://{{endpoint.base_url}}'
\t\tif 'www-net' in endpoint.base_url:
\t\t\tself.base_url = f'{{self.api_root}}/api/{{endpoint.version}}'
\t\telse:
\t\t\tself.base_url = f'{{self.api_root}}/{{endpoint.version}}'
\t\ttry:
\t\t\tif update_check:
\t\t\t\tversions = requests.get(self.api_root).json()[0]
\t\t\t\tcur_v = None
\t\t\t\tref = [int(r) for r in endpoint.version.split('.')]
\t\t\t\tfor v in versions:
\t\t\t\t\tif v['major'] == ref[0] and v['minor'] == ref[1]:
\t\t\t\t\t\tif self._build_version != v['numeric']:
\t\t\t\t\t\t\tlogger.warning(f"This library was built for NETDB API version {{self._build_version}}, but API reports {{v['numeric']}}. Please consider updating. "
\t\t\t\t\t\t                "(You can disable this message by setting NETDB_CLIENT_DISABLE_UPDATE_CHECK in your environment or by passing update_check=False to APISession)")
\t\t\t\t\t\tbreak
\t\texcept:
\t\t\tpass

\tdef execute_ta(self, ta: list, dry_mode: bool = False, dict_mode: bool = False, su_login_name: str = None) -> Union[list, dict]:
\t\tres = self.session.post(f'{{self.base_url}}/wapi/transaction/execute?dry_mode={{str(dry_mode).lower()}}&dict_mode={{str(dict_mode).lower()}}{{"" if su_login_name is None else "&su_login_name="+su_login_name}}', json=ta)
\t\tif not res.ok:
\t\t\traise ExecutionError(res.status_code, res.reason, res.json() if "json" in res.headers["content-type"] else res.text)
\t\treturn res.json()

\tdef close(self):
\t\tself.session.close()


def _denullify_dict(d):
\td = {{k:v for k,v in d.items() if v is not None}}
\tfor k,v in d.items():
\t\tif isinstance(v, ExplicitNull):
\t\t\td[k] = None
\treturn d
""")

    class_bodies = {k: f"""
@dataclass()
class {camel_case(v.name)}(APIObject):
""" for k, v in loader.api_objects.items()}

    # generate fields
    for k, v in loader.api_objects.items():
        for p in v.attributes.values():
            class_bodies[
                k] += f"""\t{replace_keywords(p.name)}: {translate_json_data_type(p.data_type['json_name'])}\n"""

    # generate __post_init__
    for k, v in loader.api_objects.items():
        class_bodies[k] += f"""\n\tdef __post_init__(self):\n"""
        class_bodies[k] += generate_object_type_checks(v)
    # generate functions
    for f in loader.api_functions:
        if f.name == 'document':
            continue  # FIXME
        if f"{f.system}.{f.object_type}" not in class_bodies:
            if not f.is_returning:
                continue
            if strict:
                raise ValueError(
                    f"'{f.system}.{f.object_type}' not found in object index (looking for definition because function {f.system}.{f.object_type}.{f.name} is returning)! (ignore this error with --skip-errors)")
            logging.warning(f"'{f.system}.{f.object_type}' not found in object index!")
            continue
        params = sorted([
            f"{replace_keywords(p.name)}_new: {translate_json_data_type(p.data_type['json_name']) if not p.new.is_nullable else 'Union[' + translate_json_data_type(p.data_type['json_name']) + ', ExplicitNull]'}{gen_def(p, 'new')}"
            for p in f.parameters.values() if p.new is not None],
            key=lambda item: 'Z' if '=' in item else 'A')
        old_params = sorted([
            f"{replace_keywords(p.name)}_old: {translate_json_data_type(p.data_type['json_name'])}{gen_def(p, 'old')}"
            for p in f.parameters.values() if p.old is not None],
            key=lambda item: 'Z' if '=' in item else 'A')
        params.extend(old_params)
        params = sorted(params, key=lambda item: 'Z' if '=' in item else 'A')
        dict_new_params_identity = [f"'{replace_keywords(p.name)}': {replace_keywords(p.name)}_new" for p in
                                    f.parameters.values() if p.new is not None]
        dict_old_params_identity = [f"'{replace_keywords(p.name)}': {replace_keywords(p.name)}_old" for p in
                                    f.parameters.values() if p.old is not None]
        new_params_identity = [f"{replace_keywords(p.name)}_new={replace_keywords(p.name)}_new" for p in
                               f.parameters.values() if p.new is not None]
        old_params_identity = [f"{replace_keywords(p.name)}_old={replace_keywords(p.name)}_old" for p in
                               f.parameters.values() if p.old is not None]
        class_bodies[f'{f.system}.{f.object_type}'] += f"""
\t@staticmethod
\tdef {f.name}_ta({', '.join(params)}) -> dict:
{generate_function_type_checks(f)}
\t\treturn {{'name': '{f.system}.{f.object_type}.{f.name}', 'new': _denullify_dict({{{', '.join(dict_new_params_identity)}}}), 'old': _denullify_dict({{{', '.join(dict_old_params_identity)}}})}}

\t# TODO: Consider is_returning
\t@classmethod
\tdef {f.name}(cls, api_session: APISession, {', '.join(params)}) -> list:
{generate_function_type_checks(f)}
\t\tres = api_session.session.post(api_session.base_url + '/wapi/transaction/execute', json=[cls.{f.name}_ta({', '.join(new_params_identity)}{', ' if len(new_params_identity) > 0 else ''}{', '.join(old_params_identity)})])
\t\tif not res.ok:
\t\t\traise ExecutionError(res.status_code, res.reason, res.json() if "json" in res.headers["content-type"] else res.text)
\t\treturn [cls(**s) for s in res.json()[0]]
"""

    # dump
    files = {}
    for (obj, code) in class_bodies.items():
        sys = obj.split('.')[0]
        if sys not in files:
            files[sys] = open(os.path.join(output_dir, sys + '.py'), 'w+')
            files[sys].write("""### AUTOGENERATED
from . import _denullify_dict
from . import *
from dataclasses import dataclass
from typing import Union

""")
        files[sys].write(code)
    for f in files.values():
        f.close()


@cli.command(name='es-webpack')
@click.option('--output-dir', default='src/api-services.gen/')
def es_webpack(output_dir):
    keyword_replacements = {'class': 'cls'}
    files = {}
    for f in loader.api_functions:
        if f.name == 'document':
            continue  # FIXME
        fname = f'{f.system}.{f.object_type}.js'
        f_handle = files.get(fname, None)
        if fname not in files:
            files[fname] = open(os.path.join(output_dir, fname), 'w')
            f_handle = files[fname]
            f_handle.write("""// -- AUTOGENERATED --

// eslint-disable-next-line no-unused-vars
import APIUtils from '@/util/apiutil'

import Axios from 'axios'
import NETVSConfig from '@/../netvs.config'

/* eslint-disable camelcase */
/* eslint-disable quote-props */
/* eslint-disable object-curly-spacing */
/* eslint-disable array-bracket-spacing */
/* eslint-disable object-shorthand */
export default {
""")
        params_str = ''
        params_dict = '{ '
        params_dict_new = '{ '
        params_dict_old = '{ '
        for (p, v) in f.parameters.items():
            old_def_def = False
            new_def_def = False
            old_default = None
            new_default = None
            p_esc = p
            for (unesc, esc) in keyword_replacements.items():
                p_esc = p_esc.replace(unesc, esc)
            if v.old is not None:
                old_def_def = v.old.data_default is not None
                if old_def_def:
                    old_default = v.old.data_default
                    if v.data_type['json_name'] == 'boolean':
                        old_default = str(old_default).lower()
            if v.new is not None:
                new_def_def = v.new.data_default is not None
                if new_def_def:
                    new_default = v.new.data_default
                    if v.data_type['json_name'] == 'boolean':
                        new_default = str(new_default).lower()
            if v.data_type['json_name'] == 'string':
                if old_default is not None:
                    old_default = '\'{}\''.format(old_default)
                if new_default is not None:
                    new_default = '\'{}\''.format(new_default)
            if v.old is not None:
                if not f.is_data_manipulating and v.data_type['json_name'] == 'array':
                    params_dict_old += '\'{p}\': ({p_esc}_old === null) ? null : JSON.stringify({p}_old), '.format(
                        p_esc=p,
                        p=p)
                else:
                    params_dict_old += '\'{p}\': {p_esc}_old, '.format(p=p, p_esc=p_esc)
                if old_def_def:
                    params_str += '{p_esc}_old = {d}, '.format(p_esc=p_esc, p=p, d=old_default)
                else:
                    params_str += '{p_esc}_old, '.format(p_esc=p_esc, p=p)
            if v.new is not None:
                if new_def_def:
                    params_str += '{p}_new = {d}, '.format(p=p, d=new_default)
                else:
                    params_str += '{p}_new, '.format(p=p)
                params_dict_new += '\'{p}\': {p}_new, '.format(p=p)

        params_dict_new += '}'
        params_dict_old += '}'

        if not f.is_data_manipulating:
            params_dict_old = params_dict_old.replace('_old', '')
            params_str = params_str.replace('_old', '')

        params_dict += '\'new\': '
        params_dict += params_dict_new
        params_dict += ', '
        params_dict += '\'old\': '
        params_dict += params_dict_old
        params_dict += '}'
        params_dict = params_dict.replace(', }', '}')
        params_dict_old = params_dict_old.replace(', }', '}')
        params_list = '{\'old\': '
        params_list += re.sub(r': [a-zA-Z0-9_]+', '', params_dict_old.replace('{', '[').replace('}', ']')).replace(',]',
                                                                                                                   ']')
        params_list += ', \'new\': '
        params_list += re.sub(r': [a-zA-Z0-9_]+', '', params_dict_new.replace('{', '[').replace('}', ']'))
        params_list += '}'
        params_list = params_list.replace(', ]', ']')

        params_str = params_str[:-2]
        params_str = params_str.replace('None', 'null')
        func_str = None
        if f.is_data_manipulating:
            func_str = """
  {name}ParamsList () {{
    return {params_list}
  }},
  {name} (config, {{{params}}}) {{
    const params = {params_dict}
    // TODO: Return ta-object instead
    return Axios.post(`${{NETVSConfig.NETDB_API_BASE_URL}}/${{NETVSConfig.NETDB_API_VERSION}}/{fq_name_slash}`, params, (config || {{}}).netdb_axios_config)
  // eslint-disable-next-line comma-dangle
  }},"""
        else:
            func_str = """
  {name} (config, {{ {params} }}) {{
    const params = APIUtils.denullify_dict({params_dict_old})
    const cnf = {{}}
    Object.assign(cnf, (config || {{}}).netdb_axios_config)
    cnf.params = params
    return Axios.get(`${{NETVSConfig.NETDB_API_BASE_URL}}/${{NETVSConfig.NETDB_API_VERSION}}/{fq_name_slash}`, cnf)
  // eslint-disable-next-line comma-dangle
  }},"""
        f_handle.write(
            func_str.format(params_list=params_list, params_dict_old=params_dict_old, params_dict=params_dict,
                            name=f.name, params=params_str,
                            fq_name_slash=f.fq_name.replace('.', '/')))

    for f in files.values():
        f.write("""
}
""")
        f.close()


@cli.command()
def ta_schema():
    print(yaml.dump(loader.version_detail['transaction_json_schema']))


@cli.command()
@click.option('--package', help='GOLang Package name', default=None)
@click.option('--output', '-o', help='Output File', default=None)
def golang_structs(package, output):
    target_file = output
    if output is not None:
        output = open(output, 'w')
    def netdb_datatype_to_go_ptr(a):
        if a.is_nullable:
            return f'*{netdb_datatype_to_go(a.data_type)}'
        return netdb_datatype_to_go(a.data_type)

    def netdb_datatype_to_go(n):
        if n['name'] == 'integer8':
            return 'int64'
        if n['name'] == 'integer4u':
            return 'uint32'
        if n['name'] == 'integer4':
            return 'int32'
        if n['name'] == 'integer1u':
            return 'uint8'
        if n['name'] == 'integer2u':
            return 'uint16'
        if n['name'] == 'tuple_ntab_bynum':
            return 'int64'
        if n['name'] == 'boolean':
            return 'bool'
        if n['name'] in ['uuid', 'text_short', 'text', 'text_kw_mc_short', 'fqdn', 'text_kw_lc_long',
                         'text_sql_identifier', 'text_kw_mc_long', 'fqdn_label', 'text_long', 'text_kw_uc_short',
                         'text_ot_identifier', 'text_kw_lc_short', 'tuple_ntab_bystr', 'role_fq_name', 'text_detailed',
                         'interval', 'json', 'text_sep', 'text_ascii', 'host_oper_mode', 'text_otfunc_identifier',
                         'text_kvkw', 'text_tag']:
            return 'string'
        if n['name'] == 'bytea':
            return 'string'
        if n['name'] in ['uuid_array', 'text_array', 'fqdn_array']:
            return '[]string'
        if n['name'] in ['json_array']:
            return '[]interface{}'
        if n['name'] in ['geo_loc']:
            return 'map[string]float64'
        if n['name'] in ['json_object']:
            return 'map[string]interface{}'
        if n['name'] == 'ip_network':
            return 'net.IPNet'
        if n['name'] == 'ip_network_array':
            return '[]net.IPNet'
        if n['name'] == 'ip_address':
            return 'net.IPAddr'
        if n['name'] == 'date':
            return 'string'  # FIXME
        if n['name'] == 'timestamp_tz':
            return 'string'  # FIXME
        if n['name'] in ['mac_address8', 'mac_address']:
            return 'net.HardwareAddr'
        if n['name'] == 'integer2_array':
            return '[]int16'
        if n['name'] == 'integer8_array':
            return '[]int64'
        if n['name'] == 'json_array_of_objects':
            return '[]map[string]interface{}'

        # Fallback
        print(f'Unknown datatype "{n["name"]}". Using json_name...', file=sys.stderr)
        if n['json_name'] == 'string':
            return 'string'
        print(f'Unknown datatype "{n["name"]}" (def: {n})! Exiting.', file=sys.stderr)
        exit(1)

    if package is not None:
        print(f"package {package}\n", file=output)
    print('import "net"\n', file=output)
    for o in loader.api_objects.values():
        cap_fq = ''.join([t[0].upper() + t[1:] if len(t) >= 2 else t.upper() for t in o.fq_name.split('.')])
        cap_fq = ''.join([t[0].upper() + t[1:] if len(t) >= 2 else t.upper() for t in cap_fq.split('_')])
        print(f'''type {cap_fq} struct {{''', file=output)
        space_alignment_type = 1
        space_alignment_anno = 1
        for a in o.attributes.keys():
            a = a.replace('.', '')
            a = a.replace('_', '')
            if len(a) > space_alignment_type:
                space_alignment_type = len(a)
        for a in o.attributes.values():
            le = len(netdb_datatype_to_go_ptr(a))
            if le > space_alignment_anno:
                space_alignment_anno = le
        space_alignment_anno = max(space_alignment_anno, 1)
        space_alignment_type = max(space_alignment_type, 1)
        for a_name, a in o.attributes.items():
            cap_fq = ''.join([t[0].upper() + t[1:] if len(t) >= 2 else t.upper() for t in a_name.split('_')])
            print(
                f'\t{cap_fq}{" " * (space_alignment_type - len(cap_fq))} '
                f'{netdb_datatype_to_go_ptr(a)}'
                f'{" " * (space_alignment_anno - len(netdb_datatype_to_go_ptr(a)))} '
                f'`mapstructure:"{a_name}"`', file=output)
        print('}\n', file=output)
    if output is not None:
        output.close()


@cli.command()
@click.option('--default_endpoint', default='test')
def openapi(default_endpoint):
    def setDef(tmp):
        res = []
        for r in tmp:
            if 'schema' in r:
                if not r['schema']['has_def']:
                    del (r['schema']['default'])
                del (r['schema']['has_def'])
                res.append(r)
            else:
                if not r['has_def']:
                    del (r['default'])
                del (r['has_def'])
                res.append(r)
        return res

    def gen_param_defs(d, scope):
        defs = {
            'type': d.data_type['json_name'],
            'description': d.description_detail if d.description_detail is not None else '',
            'nullable': d.__dict__[scope].is_nullable,
            'has_def': d.__dict__[scope].data_default is not None,
            'default': d.__dict__[scope].data_default
        }
        if d.data_type['json_name'] == 'array':
            defs['items'] = {'type': 'string'}
        elif d.data_type['json_name'] == 'json':
            del defs['type']
        return defs

    def generateParameters(f: ApiFunction, forceQuery=False):
        if not f.is_data_manipulating or forceQuery:
            tmp = [
                {
                    'name': p, 'description': d.description_detail if d.description_detail is not None else '',
                    'required': d.old.is_required if d.old is not None else d.new.is_required,
                    'schema': {
                        'type': d.data_type['json_name'],
                        'nullable': d.old.is_nullable if d.old is not None else d.new.is_nullable,
                        'has_def': d.old.data_default is not None if d.old is not None else d.new.data_default is not None,
                        'default': d.old.data_default if d.old is not None else d.new.data_default
                    },
                    'in': 'query'
                }
                for p, d in f.parameters.items()]
            for p in tmp:
                if p['schema']['type'] == 'array':
                    # TODO!
                    p['schema']['type'] = 'string'
                    p['schema']['format'] = '["value1","value2",...]'
                elif p['schema']['type'] == 'json':
                    del p['schema']['type']
                if f.parameters[p['name']].supported_values is not None:
                    p['description'] += '\n\n' + '\n'.join(
                        [k + ': ' + ((v.get('en', v['de']) if isinstance(v, dict) else v) or '') for k, v in
                         f.parameters[p['name']].supported_values.items()])
                    p['schema']['enum'] = list(f.parameters[p['name']].supported_values.keys())
            return setDef(tmp)
        new_params = {
            p: gen_param_defs(d, 'new')
            for p, d in f.parameters.items() if d.new is not None
        }

        old_params = {
            p: gen_param_defs(d, 'old')
            for p, d in f.parameters.items() if d.old is not None
        }
        old_params = {k: setDef([v])[0] for k, v in old_params.items()}
        new_params = {k: setDef([v])[0] for k, v in new_params.items()}
        props = {}
        if len(old_params) > 0:
            props['old'] = {
                'description': 'Alte Attribute zur einduetigen Identifizierung des Objekts',
                'type': 'object',
                'properties': old_params,
            }
            req = [p for p, d in f.parameters.items() if d.old is not None and d.old.is_required]
            if len(req) > 0:
                props['old']['required'] = req
        if len(new_params) > 0:
            props['new'] = {
                'description': 'Neue Angaben',
                'type': 'object',
                'properties': new_params,
            }
            req = [p for p, d in f.parameters.items() if d.new is not None and d.new.is_required]
            if len(req) > 0:
                props['new']['required'] = req
        return {
            'required': True,
            'content': {
                'application/json': {
                    'schema': {
                        'type': 'object',
                        'properties': props
                    }
                }
            }
        }

    def renderDataType(d):
        dat = {'type': d.data_type['json_name'],
               'description': d.description_detail if d.description_detail is not None else '',
               }
        if d.data_type['format_literal'] is not None and d.data_type['json_name'] == 'string':
            dat['format'] = d.data_type['format_literal']
        elif d.data_type['name'] == 'text_array':
            dat['items'] = {
                'type': 'string'
            }
        elif re.match(r'^integer.*_array$', d.data_type['name']) is not None:
            dat['items'] = {
                'type': 'integer'
            }
        elif d.data_type['json_name'] == 'array':
            dat['items'] = {
                'type': 'string'
            }
        elif d.data_type['json_name'] == 'json':
            del dat['type']
        return dat

    paths = {f"/{f.system}/{f.object_type}/{f.name}":
        {
            'post' if f.is_data_manipulating else 'get': {
                'requestBody' if f.is_data_manipulating else 'parameters': generateParameters(f),
                'tags': [f.system],
                'security': [{'api_key': []}],
                'responses': {
                    200: {
                        'description': 'Request erfolgreich',
                        'content': {
                            'application/json': {
                                'schema': {
                                    'type': 'array',
                                    'items': {
                                        'type': 'array',
                                        'items': {
                                            '$ref': '#/components/schemas/' + f.system + '.' + f.object_type
                                        }
                                    }
                                }
                            }} if f.is_returning else None},
                    400: {
                        'description': 'Eingabefehler',
                    },
                    401: {
                        'description': 'Unautorisiert',
                    },
                    500: {
                        'description': 'Interner Serverfehler',
                    },
                }}}
        for f in loader.api_functions if f.is_executable and
                                         not (
                                                 f.system == 'wapi' and f.object_type == 'transaction' and f.name == 'execute')}
    for p in paths.values():
        scheme = 'get'
        if 'post' in p:
            scheme = 'post'
        if p[scheme]['responses'][200]['content'] is None:
            del p[scheme]['responses'][200]['content']
    wapi_function = [f for f in loader.api_functions if
                     (f.system == 'wapi' and f.object_type == 'transaction' and f.name == 'execute')][0]
    paths['/wapi/transaction/execute'] = {
        'post': {
            'parameters': generateParameters(wapi_function, True),
            'requestBody':
                {
                    'required': True,
                    'content': {
                        'application/json': {
                            'schema': {'$ref': (
                                f'https://netvs{"-devel" if "devel" in loader.api_host else ""}.scc.kit.edu/netdb_{loader.api_version.replace(".", "_")}_ta.yml') if
                            loader.version_detail['major'] >= 4 else '#/components/schemas/wapi.transaction_stmt'}
                        }
                    }
                },
            'tags': ['wapi'],
            'security': [{'api_key': []}],
            'responses': {
                200: {
                    'description': 'Request erfolgreich',
                    'content': {
                        'application/json': {
                            'schema': {
                                'type': 'array',
                                'items': {
                                    'type': 'array',
                                    'items': {
                                        'type': 'object'
                                    }
                                }
                            }
                        }}
                },
                400: {
                    'description': 'Eingabefehler',
                },
                401: {
                    'description': 'Unautorisiert',
                },
                500: {
                    'description': 'Interner Serverfehler',
                },
            }
        }}
    servers = [
        {
            'url': f'https://api.netdb-test.scc.kit.edu/{loader.api_version}',
            'description': 'Test-Server. Keine Auswirkungen auf die Dienste.'
        },
        {
            'url': f'https://api.netdb.scc.kit.edu/{loader.api_version}',
            'description': 'Produktiv-Server'
        },
        {
            'url': f'https://api.netdb-devel.scc.kit.edu/{loader.api_version}',
            'description': 'Devel-Server. Instabile Entwicklungsumgebung.'
        }
    ]
    if default_endpoint == 'prod':
        tmp = servers[1]
        del servers[1]
        servers.insert(0, tmp)
    elif default_endpoint == 'devel':
        tmp = servers[2]
        del servers[2]
        servers.insert(0, tmp)
    tags_red = []
    for s in loader.systems:
        tags_red.append({'name': s.name, 'description': s.description if s.description is not None else s.language_dict['description'][loader.lang]})
    swagger_base = {
        'openapi': '3.0.3', 'info': {
            'contact': {'name': 'NETVS Support', 'email': 'netvs@scc.kit.edu'},
            'description': """Die Schnittstelle WebAPI bietet IT-Betreuern am KIT die Möglichkeit, eigene, speziell zugeschnittene Anwendungsprogramme für die Pflege ihrer netzwerk-spezifischen Anwendungsdaten (wie z.B. DNS) aufzubauen.
Die Schnittstelle setzt auf der Netzdatenbank des SCC (NetDB) auf und ist für eine vollautomatisierte Nutzung auf Basis des heutzutage üblichen textbasierten Datenaustauschformates JSON (JavaScript Object Notation) vorgesehen (s.a. http://de.wikipedia.org/wiki/JavaScript_Object_Notation).
Sie ist über HTTPS erreichbar und kann dadurch mit jeder beliebigen HTTP-Programmbibliothek benutzt werden. Im Rahmen der implementierten Systeme (Anwendungsbereiche), Objekttypen und Funktionen lassen sich sämtliche Daten durch entsprechende Dienstanforderungen (Requests) sowohl ausgeben bzw. abfragen als auch manipulieren (im Standardfall Eintragen, Ändern, Löschen). Die konkreten, jeweils versionsspezifischen Informationen und Parameter zu diesen Systemen, Objekttypen und Funktionen sind nicht Bestandteil dieser Dokumentation, sondern über selbstdokumentierende Indexabfragen der WebAPI erreichbar.""",
            'version': loader.version_detail['numeric'],
            'title': 'SCC NETDB-API'}, 'externalDocs': {
            'description': 'Weitere Dokumentation',
            'url': loader.version_detail['doc_uri'],
        },
        'servers': servers,
        'tags': tags_red,
        'paths': paths,
        # '$defs': loader.version_detail['transaction_json_schema']['$defs'],
        'components': {
            'securitySchemes': {'api_key': {'type': 'apiKey', 'name': 'Authorization', 'in': 'header'}},
            'schemas': {o.fq_name:
                {
                    'type': 'object',
                    'description': o.description_detail if o.description_detail is not None else '',
                    'properties': {
                        a: renderDataType(d)
                        for a, d in o.attributes.items()
                    }
                }
                for o in loader.api_objects.values()}
        }

    }
    yaml.add_representer(System, representer=APIBaseObject.yaml_representer)
    print(yaml.dump(swagger_base))
