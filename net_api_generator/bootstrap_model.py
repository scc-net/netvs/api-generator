from dataclasses import dataclass


class APIBaseObject(object):
    @staticmethod
    def yaml_representer(dumper, data):
        return dumper.represent_dict(data.__dict__)


@dataclass()
class APIObject(APIBaseObject):
    name: str
    system: str
    fq_name: str
    attributes: dict
    is_log_dst: bool
    is_log_src: bool
    constraints: dict
    referencing: dict
    referenceable: dict
    __data_types__: dict
    description_title: str = None
    description_abbrev: str = None
    description_detail: str = None
    is_lang_dst: bool = None
    is_lang_src: bool = None
    is_otattr_dst: bool = None
    is_otattr_src: bool = None
    do_activate_global_pk2obj: bool = None
    object_type_grants_read_access: bool = None

    def __post_init__(self):
        if isinstance(list(self.attributes.values())[0], dict):
            self.attributes = {k: ObjectAttribute(__data_types__=self.__data_types__, name=k, **v) for k, v in
                               self.attributes.items()}


@dataclass()
class ObjectAttribute(APIBaseObject):
    name: str
    is_core: bool
    data_type: dict
    is_nullable: bool
    __data_types__: dict
    description_detail: str = None
    description_sys_scope: str = None
    description_obj_type_scope: str = None
    json_data_type: str = None
    is_deprecated: bool = None
    supported_values: str = None

    def __post_init__(self):
        if not isinstance(self.data_type, dict):
            self.data_type = self.__data_types__[self.data_type]


@dataclass()
class System(APIBaseObject):
    name: str
    description: str = None
    language_dict: dict = None


@dataclass()
class FunctionParameterScope(APIBaseObject):
    is_nullable: bool
    is_required: bool
    data_default: str = None


@dataclass()
class FunctionParameter(APIBaseObject):
    name: str
    data_type: dict
    __data_types__: dict
    description_detail: str = None
    description_sys_scope: str = None
    description_obj_type_scope: str = None
    json_data_type: str = None
    is_deprecated: bool = None
    supported_values: dict = None
    new: FunctionParameterScope = None
    old: FunctionParameterScope = None

    def __post_init__(self):
        if isinstance(self.new, dict):
            self.new = FunctionParameterScope(**self.new)
        if isinstance(self.old, dict):
            self.old = FunctionParameterScope(**self.old)
        if not isinstance(self.data_type, dict):
            self.data_type = self.__data_types__[self.data_type]


@dataclass()
class ApiFunction(APIBaseObject):
    name: str
    system: str
    fq_name: str
    parameters: dict
    object_type: str
    is_returning: bool
    is_executable: bool
    is_data_manipulating: bool
    __data_types__: dict
    is_returning_referenceable: bool = None

    def __post_init__(self):
        if isinstance(list(self.parameters.values())[0], dict):
            self.parameters = {k: FunctionParameter(__data_types__=self.__data_types__, name=k, **v) for k, v in
                               self.parameters.items()}
